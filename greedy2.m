function m = greedy(i,vmap,robots)
    map = vmap(:,:,i);
    
    for j = 1:length(robots) % set occupied tiles as wall (avoid greedy bug)
        map(robots(j,1),robots(j,2)) = 255;
    end
    
    l = length(map);
    D = Inf*ones(l); % temporary support map
    f(1,:) = robots(i,:); % seed for frontier exploration
    frontier = [];
    nf = []; % new border
    D( f(1,1), f(1,2) ) = 1; % starting point
    
    d = 1; % steps counter
    found = false;
    while not(found)
        
        for j = 1:length(f(:,1)) % BFS on all the borders
            
            % check nearest point
            for x=-1:1
                for y=-1:1
                    nx = f(j,1)+x;
                    ny = f(j,2)+y;
                    
                    if and( nx > 0, nx <= l)
                        if and(ny > 0, ny <=l) % if is not out of borders
                            if map(nx,ny) == -1 % Check if we reach a unexplored point
                                frontier = [frontier; [nx, ny, d]];
                                found = true;
                            else
                                if not(map(nx,ny) == 255) % check if is crossable
                                    if D( nx, ny ) > d+1 % if i found a minimal path
                                        D( nx, ny ) = d+1;
                                        if length(nf) == 0 % Per risolvere il BUG del 0,0
                                            nf = [nx ny];
                                        else
                                            nf(length(nf(:,1))+1,:) = [nx ny]; % generate the new borders
                                        end
                                    end
                                else
                                    D(nx,ny) = Inf; % Segna barriera
                                end
                            end
                        end
                    end
                end
            end
        end
        f = nf;
        if length(f) == 0 % Se non ci sono altri punti da esplorare interrompi ricerca
            break;
        end
        
        nf = [];
        d = d +1;
    end
    
    % Here we chose the point of frontier where to go
    m = robots(i,:); % nel caso l'algoritmo non trovi altri punti rimane fermo
    if length(frontier) > 0
        [d j] = min(frontier(:,3));% scelgo il punto di frontiera a distanza minima
        m = DJ(map,robots(i,:), frontier(j,1:2));
    end
end