% Collaborative multi-robot exploration and mapping

% Author: Dennj Osele
% dennj.osele@gmail.com
% 2017




function varargout = Simulator(varargin)
    % SIMULATOR MATLAB code for Simulator.fig
    %      SIMULATOR, by itself, creates a new SIMULATOR or raises the existing
    %      singleton*.
    %
    %      H = SIMULATOR returns the handle to a new SIMULATOR or the handle to
    %      the existing singleton*.
    %
    %      SIMULATOR('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in SIMULATOR.M with the given input arguments.
    %
    %      SIMULATOR('Property','Value',...) creates a new SIMULATOR or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before Simulator_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to Simulator_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES
    
    % Edit the above text to modify the response to help Simulator
    
    % Last Modified by GUIDE v2.5 24-May-2016 05:34:43
    
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
        'gui_Singleton',  gui_Singleton, ...
        'gui_OpeningFcn', @Simulator_OpeningFcn, ...
        'gui_OutputFcn',  @Simulator_OutputFcn, ...
        'gui_LayoutFcn',  [] , ...
        'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end
    
    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
    
    
    % --- Executes just before Simulator is made visible.
    clc
    
function Simulator_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to Simulator (see VARARGIN)
    
    % Choose default command line output for Simulator
    handles.output = hObject;
    
    % Update handles structure
    guidata(hObject, handles);
    
    % UIWAIT makes Simulator wait for user response (see UIRESUME)
    % uiwait(handles.figure1);
    
    
    % --- Outputs from this function are returned to the command line.
function varargout = Simulator_OutputFcn(hObject, eventdata, handles)
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Get default command line output from handles structure
    varargout{1} = handles.output;
    
    
    % --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
    % hObject    handle to popupmenu1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
    %        contents{get(hObject,'Value')} returns selected item from popupmenu1
    
    
    % --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to popupmenu1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: popupmenu controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on slider movement.
function sliderN_Callback(hObject, eventdata, handles)
    % hObject    handle to sliderN (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    handles.textN.String = round(handles.sliderN.Value);
    
    % --- Executes during object creation, after setting all properties.
function sliderN_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to sliderN (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
    % --- Executes on slider movement.
function slidervision_Callback(hObject, eventdata, handles)
    % hObject    handle to slidervision (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    handles.textvision.String = round(handles.slidervision.Value);
    
    
    
    % --- Executes during object creation, after setting all properties.
function slidervision_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to slidervision (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
    % --- Executes on slider movement.
function slidercom_Callback(hObject, eventdata, handles)
    % hObject    handle to slidercom (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    handles.textcom.String = round(handles.slidercom.Value);
    
    
    
    % --- Executes during object creation, after setting all properties.
function slidercom_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to slidercom (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
    % --- Executes on button press in btstart.
function btstart_Callback(hObject, eventdata, handles)
    % hObject    handle to btstart (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % Frontier Exploration
    
    %% muri
    % -1 blocks of unexplored world
    % 0 explored blocks
    % 255 walls
    
    unexplored = -1;
    explored = 0;
    wall = 255;
    
    % load blank map
    l = round(handles.slidermap.Value);
    map = zeros(l);
    map(1,:) = 1;
    map(:,1) = 1;
    map(end,:) = 1;
    map(:,end) = 1;
    
    % If selected map
    if handles.rbLoadMap.Value
        map = double(imread(['maps/' char(handles.menumap.String(handles.menumap.Value))])); % load map
        l = size(map(:,:),1); % world dimension
    end
    
    
    %% Robots and Environment Paramenters
    vision = round(handles.slidervision.Value); % vision range
    comrange = round(handles.slidercom.Value); % comunication range
    N = round(handles.sliderN.Value); % numbero of robot explorers
    
    if handles.rbrandom.Value == 1 % Positions
            robots = 1+randi(l-2,N,2); % robot explorers coordinates in the map frame reference
    elseif handles.rbcenter.Value == 1
        robots = ones(N,2)*round(l/2);
    else
        robots = ones(N,2)*3;
    end
           
    vmap = -ones(l,l,N); % virtual maps of the world; One for each robot

    graph = 0;
    counter = 0;
    
    f = figure('units','normalized','outerposition',[0 0 1 1]) % set figure fullscreen
    f.Name = 'Simulation is running';
    exe = uicontrol(f,'Style','checkbox',...
        'String','Exit',...
        'Value',0,'Position',[15 15 200 25]);
    exe.FontSize = 25;
    
    
    while and(exe.Value == 0, max(min(min(vmap(:,:,:)))) == unexplored) % stop execution when the first robot complete mapping
        
        % Robots Code start here
        for i=1:N % for each robot execute code in sequential way
            if min(min(vmap(:,:,i))) == unexplored % check if in the virtual map contains unexplored areas
                
                %% Algorithms
                if handles.selectalg.Value == 1
                    m = greedy(i,vmap,robots);
                else
                    m = greedy2(i,vmap,robots);
                end;
                
                % check if next tile is empty
                isempty = true;
                for n = 1:N
                    if robots(n,:) == m % check if some other robot is in the tile
                        isempty = false;
                    end
                end
                if isempty % if tile is empty, robot(i) move in it, else wait
                    robots(i,:) = m;
                end
                
                %% Update virtual map
                for x=-vision:vision
                    for y=-vision:vision
                        nx = m(1) + x;
                        ny = m(2) + y;
                        if and( nx > 0, nx <= l)
                            if and(ny > 0, ny <=l) % if is not out of borders
                                [mm dd] = DJ(vmap,m,[nx,ny]); % to avoid robots to see through the walls
                                if dd <= vision
                                    vmap( nx,ny,i ) = map( nx,ny );
                                end
                            end
                        end
                    end
                end
                
                %% Merge map with robot in the comunications range
                for i = 1:N
                    for j = 1:N
                        if norm(robots(i,:)-robots(j,:)) <= comrange
                            % Merge map
                            vmap(:,:,i) = max(vmap(:,:,i),vmap(:,:,j));
                            vmap(:,:,j) = vmap(:,:,i); % share merged map
                        end
                    end
                end             
            end
        end
        
        
        %% Generate image from vmap of robot(1)
        rmap = -ones(l,l); % map of robots position
        for i=1:N
            rmap( robots(i,1),robots(i,2) ) = 3; % set robot explorer
        end
        
        rmap(1,1) = 4;
        
        r = floor(sqrt(N));
        c = ceil(N/r);
        for i = 1:N
            subplot(r,c,i);
            image(max(max(vmap(:,:,i)*Inf,vmap(:,:,i)),rmap),'CDataMapping','scaled')
            hold on
            scatter(robots(i,2),robots(i,1));
            
            daspect([1 1 400]) % fixed axis ratio
        end
        
        pause(.2);
        graph(counter+1) = 100-100*length(find(vmap == -1))/(l^2*N);
        counter = counter +1;
    end
    
    if exe.Value == 0
        subplot(1,1,1)
        plot(graph)
        title(['Simulation completed in ' num2str(counter) ' steps'])
        xlabel('Number of steps')
        ylabel('Percentage of explored map')
        exe.Visible = 'off';        
       % uiwait(msgbox(['Simulation completed in ' num2str(counter) ' steps']))
    end
    
    if exe.Value == 1
        close
    end
    
    % --- Executes on selection change in selectalg.
function selectalg_Callback(hObject, eventdata, handles)
    % hObject    handle to selectalg (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: contents = cellstr(get(hObject,'String')) returns selectalg contents as cell array
    %        contents{get(hObject,'Value')} returns selected item from selectalg
    
    
    % --- Executes during object creation, after setting all properties.
function selectalg_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to selectalg (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: popupmenu controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on slider movement.
function slidermap_Callback(hObject, eventdata, handles)
    % hObject    handle to slidermap (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    handles.tbmapsize.String = num2str(round(handles.slidermap.Value));
    
    
    % --- Executes during object creation, after setting all properties.
function slidermap_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to slidermap (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
    % --- Executes on selection change in menumap.
function menumap_Callback(hObject, eventdata, handles)
    % hObject    handle to menumap (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: contents = cellstr(get(hObject,'String')) returns menumap contents as cell array
    %        contents{get(hObject,'Value')} returns selected item from menumap
    
    
    % --- Executes during object creation, after setting all properties.
function menumap_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to menumap (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: popupmenu controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on button press in rbloadmap.
function rbLoadMap_Callback(hObject, eventdata, handles)
    % hObject    handle to rbloadmap (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hint: get(hObject,'Value') returns toggle state of rbloadmap
    handles.menumap.Enable = 'on';
    handles.slidermap.Enable = 'off';
    handles.tbmapsize.Enable = 'off';
    maps = dir('maps');
    n = {maps.name};
    handles.menumap.String = n(3:end);
    
    
    
    
    % --- Executes on button press in rbBlankMap.
function rbBlankMap_Callback(hObject, eventdata, handles)
    % hObject    handle to rbBlankMap (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hint: get(hObject,'Value') returns toggle state of rbBlankMap
    handles.menumap.Enable = 'off';
    handles.slidermap.Enable = 'on';
    handles.tbmapsize.Enable = 'on';


% --- Executes on button press in rbLoadMap.
function rbloadmap_Callback(hObject, eventdata, handles)
% hObject    handle to rbLoadMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbLoadMap
    handles.menumap.Enable = 'on';
    handles.slidermap.Enable = 'off';
    handles.tbmapsize.Enable = 'off';
