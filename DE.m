% Frontier Exploration

close all
clear all
clc

%% muri
% -1 blocks of unexplored world
% 0 explored blocks
% 255 walls
unexplored = -1;
explored = 0;
wall = 255;

map = double(imread('map.bmp')); % load map
l = size(map(:,:),1); % world dimension


%% Robots and Environment Paramenters
vision = 1; % vision range
comrange = 5; % comunication range
N = 5; % numbero of robot explorers
robots = randi(l,N,2); % robot explorers coordinates in the map frame reference
rc = robots(1,:); % blind robots coordinates

vmap = -ones(l,l,N); % virtual maps of the world; One for each robot
cmap = -ones(l,l); % maps of blind robots; Shared map

figure('units','normalized','outerposition',[0 0 1 1]) % set figure fullscreen


while min(min(min(vmap(:,:,:)))) == unexplored % esegui finche c'è ancora mappa da esplorare
    
    % Robots Code start here
    for i=1:N % for each robot execute code in sequential way
        if min(min(vmap(:,:,i))) == unexplored % check if in the virtual map contains unexplored areas
            
            %% Algorithms
            %m = greedy(i,vmap,robots);
            %m = voronoiDE(i,vmap,robots);
            m = greedy2(i,vmap,robots);
            
            
            
            
            
            % QUI VA LA VOSTRA FUNZIONE
            % M = vostrafunzione(i,vmap,robots)
            
            
            
            
            
            
            % check if next tile is empty
            isempty = true;
            for n = 1:N
                if robots(n,:) == m % check if some other robot is in the tile
                    isempty = false;
                end
            end
            if isempty % if tile is empty, robot(i) move in it, else wait
                robots(i,:) = m;
            end
            
            %% Update virtual map
            for x=-vision:vision
                for y=-vision:vision
                    nx = m(1) + x;
                    ny = m(2) + y;
                    if and( nx > 0, nx <= l)
                        if and(ny > 0, ny <=l) % if is not out of borders
                            [mm dd] = DJ(vmap,m,[nx,ny]); % to avoid robots to see through the walls
                            if dd <= vision
                                vmap( nx,ny,i ) = map( nx,ny );
                            end
                        end
                    end
                end
            end
            
            %% Merge map with robot in the comunications range
            for i = 1:N
                for j = 1:N
                    if norm(robots(i,:)-robots(j,:)) <= comrange
                        % Merge map
                        vmap(:,:,i) = max(vmap(:,:,i),vmap(:,:,j));
                        vmap(:,:,j) = vmap(:,:,i); % share merged map
                    end
                end
            end
            
            % share map with blind robots
            for i = 1:N
                cmap(:,:) = max(vmap(:,:,i),cmap(:,:));
            end
        end
    end
    
    %% Blind robot control
    tmpmap = [];
    tmpmap = cmap;
    tmpmap( find(tmpmap == -1) ) = 255;
    tmpmap( find(tmpmap == 0) ) = -1;
    tmpmap( find(tmpmap == 1) ) = 0;
    rc = greedy(1,tmpmap,rc);
    cmap(rc(1),rc(2)) = 1; % percorso
    
    
    %% Generate image from vmap of robot(1)
    rmap = -ones(l,l); % map of robots position
    for i=1:N
        rmap( robots(i,1),robots(i,2) ) = 3; % set robot explorer
    end
    
    rmap( rc(1,1),rc(1,2) ) = 4; % set blind robot
    
    
    %figure(1) % Da migliorare
    r = floor(sqrt(N+1)); % plus one for blind robot
    c = ceil(N/r);
    for i = 1:N
        subplot(r,c,i);
        image(max(max(vmap(:,:,i)*Inf,vmap(:,:,i)),rmap),'CDataMapping','scaled')
        hold on
        scatter(robots(i,2),robots(i,1)); % evidenzia robot
        
        daspect([1 1 400]) % fixed axis ratio
    end
    pause(.000001);
    
    subplot(r,c,r*c); % r*c last slot
    tmpmap( find(tmpmap == 1) ) = 30;
    tmpmap( find(tmpmap == 0) ) = 20;
    %image(tmpmap,'CDataMapping','scaled')
    image(cmap,'CDataMapping','scaled')
    hold on
    scatter(rc(2),rc(1))
    daspect([1 1 400]) % fixed axis ratio
end