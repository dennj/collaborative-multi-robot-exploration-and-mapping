function m = greedy(i,vmap,robots)
    %% find nearest white tile (greedy)
    l = size(vmap(:,:,i),1);
    m = [1 1];
    dm = Inf;
    for x = 1:l
        for y = 1:l
            if vmap(x,y,i) == -1 % unexplored
                d = norm( [x y]-robots(i,:) );
                if dm >= d
                    m = [x y];
                    dm = d;
                end
            end
        end
    end
    
    % find next step to do to reach the targhet   
    m = DJ(vmap(:,:,i),robots(i,:),m);
    
    if m == robots(i,:) % if path not found move in a random direction
        t = m + randi(3,1,2) -2;
        if vmap(t(1),t(2),i) == 0 %explored % to move explored area and avoid block with wall
            m = t;
        end
    end