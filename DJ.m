%% Dijkstra
% s starting point
% e target point
% p next step to do
% d distance to target
function [p, d] = DJ(map,s,e)
    l = length(map);
    map = max(map,map * Inf); % Necessario per percorsi lunghi
    D = Inf*ones(l); % temporary support map
    f(1,:) = e; % starting border is the target point
    nf = []; % new border
    D( f(1,1), f(1,2) ) = 1; % starting point
    found = false;
    
    i = 1; % steps counter
    
    while not(found)
        
        for j = 1:length(f(:,1)) % BFS on all the borders
            
            % check nearest point
            for x=-1:1
                for y=-1:1
                    nx = f(j,1)+x;
                    ny = f(j,2)+y;
                    
                    if and(nx == s(1), ny == s(2)) % Check if we reach the starting point
                        found = true;
                        p = f(j,:); % next step
                        d = i; % distance to target
                        return;
                    else
                        if and( nx > 0, nx <= l)
                            if and(ny > 0, ny <=l) % if is not out of borders
                                if not(map(nx,ny) == Inf) % check if is crossable
                                    if D( nx, ny ) > i+1 % if i found a minimal path
                                        D( nx, ny ) = i+1;
                                        if length(nf) == 0 % Per risolvere il BUG del 0,0
                                            nf = [nx ny];
                                        else
                                            nf(length(nf(:,1))+1,:) = [nx ny]; % generate the new borders
                                        end
                                    end
                                else
                                    D(nx,ny) = Inf; % Segna barriera
                                end
                            end
                        end
                    end
                end
            end
        end
        if length(nf) == 0 % Se non trovo percorsi percorribili termina
            p = s;
            d = i;
            break;
        else
            f = nf;
            nf = [];
            i = i +1;
        end
    end
    % end Dijkstra